//template start
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <sstream>
#include <complex>
#include <vector>
#include <list>
#include <queue>
#include <deque>
#include <stack>
#include <map>
#include <set>
#include <iterator>
#include <numeric>
#include <bitset>
#include <cassert>
#include <functional>

using namespace std;
using ll = long long;
using ull = unsigned long long;
using ld = long double;

using Graph = vector<vector<ll>>;
inline ll min(ll x, ll y) { return x < y ? x : y; }
inline ll max(ll x, ll y) { return x > y ? x : y; }

#define EPS (1e-7)
#define INF (1e9)
#define PI (acos(-1))
#define rep(i, n) for (ll i = 0; i < (ll)(n); i++)
#define rep1(i, n) for (ll i = 1; i < (ll)(n); i++)
#define all(c) c.begin(), c.end()
#define pb push_back
#define fs first
#define sc second
#define show(x) cout << #x << " = " << (x) << endl
#define chmin(x, y) x = min(x, y)
#define chmax(x, y) x = max(x, y)
#define YES cout << "Yes" << endl
#define NO cout << "No" << endl
#define ANS cout << ans << endl
const int MOD = 1000000007;

//from char to int
ll ctoi(char c)
{
    ll i = c - 48;
    return i;
}
//素数判定
bool isPrime(ll X)
{
    for (ll i = 2; i <= ll(sqrt(X)); i++)
    {
        if (X % i == 0)
        {
            return false;
        }
    }
    return true;
}
//平方数判定
bool isSquare(ll N)
{
    if (ll(sqrt(N)) * ll(sqrt(N)) == N)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//各位の和の計算
ull getSumDigit(ull N)
{
    string s = to_string(N);
    ll ret = 0;
    for (int i = 0; i < s.size(); i++)
        ret += (s[i] - '0');
    return ret;
}
//ベクトルの積集合
vector<ll> setProduct(vector<ll> v1, vector<ll> v2)
{
    vector<ll> ret(0);
    for (ll i : v1)
    {
        if (find(v2.begin(), v2.end(), i) != v2.end())
        {
            ret.push_back(i);
        }
    }
    return ret;
}
//from string to vector<char>
vector<char> stov(string s)
{
    vector<char> v(all(s));
    return v;
}
//from decimal to binary
string itob(ll j)
{
    return bitset<10>(j).to_string();
}
//max of vector
ll vmax(vector<ll> v)
{
    return *max_element(all(v));
}
//min of vector
ll vmin(vector<ll> v)
{
    return *min_element(all(v));
}
//sum of vector
ll vsum(vector<ll> v)
{
    return accumulate(all(v), (ll)0);
}

// 10進数の桁数
ull getDigit(ull N)
{
    return to_string(N).length();
}

//template end

int main()
{
    //入力
    ll N, X;
    cin >> N >> X;
    vector<char> S(N);
    rep(i, N)
    {
        cin >> S[i];
    }
    //処理
    rep(i, N)
    {
        if (S[i] == 'o')
        {
            X++;
        }
        else
        {
            if (X > 0)
            {
                X--;
            }
        }
    }
    cout << X << endl;
    return 0;
}
