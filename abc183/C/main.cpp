//template start
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <string>
#include <sstream>
#include <complex>
#include <vector>
#include <list>
#include <queue>
#include <deque>
#include <stack>
#include <map>
#include <set>
#include <iterator>
#include <numeric>
#include <bitset>
#include <cassert>
#include <functional>

using namespace std;
typedef long long int ll;
using Graph = vector<vector<ll>>;
inline ll min(ll x, ll y) { return x < y ? x : y; }
inline ll max(ll x, ll y) { return x > y ? x : y; }

#define EPS (1e-7)
#define INF (1e9)
#define PI (acos(-1))
#define rep(i, n) for (ll i = 0; i < (ll)(n); i++)
#define rep1(i, n) for (ll i = 1; i < (ll)(n); i++)
#define all(c) c.begin(), c.end()
#define pb push_back
#define fs first
#define sc second
#define show(x) cout << #x << " = " << (x) << endl
#define chmin(x, y) x = min(x, y)
#define chmax(x, y) x = max(x, y)
#define YES cout << "Yes" << endl
#define NO cout << "No" << endl
#define ANS cout << ans << endl
const int MOD = 1000000007;

//from char to int
ll ctoi(char c)
{
    ll i = c - 48;
    return i;
}
//素数判定
bool is_prime(ll X)
{
    for (ll i = 2; i <= ll(sqrt(X)); i++)
    {
        if (X % i == 0)
        {
            return false;
        }
    }
    return true;
}
//平方数判定
bool is_square(ll N)
{
    if (ll(sqrt(N)) * ll(sqrt(N)) == N)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//各位の和の計算
ll sum_of_digits(ll num)
{
    ll sum = 0;
    while (num)
    {
        sum += num % 10;
        num /= 10;
    }
    return sum;
}
//ベクトルの積集合
vector<ll> set_product(vector<ll> v1, vector<ll> v2)
{
    vector<ll> ret(0);
    for (ll i : v1)
    {
        if (find(v2.begin(), v2.end(), i) != v2.end())
        {
            ret.push_back(i);
        }
    }
    return ret;
}
//from string to vector<char>
vector<char> stov(string s)
{
    vector<char> v(all(s));
    return v;
}
//from decimal to binary
string itob(ll j)
{
    return bitset<10>(j).to_string();
}
//max of vector
ll vmax(vector<ll> v)
{
    return *max_element(all(v));
}
//min of vector
ll vmin(vector<ll> v)
{
    return *min_element(all(v));
}
//sum of vector
ll vsum(vector<ll> v)
{
    return accumulate(all(v), (ll)0);
}

//template end

int main()
{
    //入力
    ll N, K;
    cin >> N >> K;
    vector<vector<ll>> T(N, vector<ll>(N));
    rep(i, N)
    {
        rep(j, N)
        {
            cin >> T[i][j];
        }
    }

    //都市1を除く都市の全順列をListに格納
    vector<vector<ll>> citiesList(0, vector<ll>(N - 1));
    vector<ll> cities(N - 1);
    for (int i = 0; i < N - 1; i++)
    {
        cities[i] = i + 2;
    }
    do
    {
        citiesList.push_back(cities);
    } while (next_permutation(all(cities)));

    //全順列についてコストを計算
    ll citiesSum;
    ll cnt = 0;
    rep(i, citiesList.size())
    {
        citiesSum = 0;
        rep(j, N - 2)
        {
            citiesSum += T[citiesList[i][j] - 1][citiesList[i][j + 1] - 1];
        }
        citiesSum += T[0][citiesList[i][0] - 1] + T[citiesList[i][N - 2] - 1][0];
        if (citiesSum == K)
        {
            cnt++;
        }
    }
    cout << cnt << endl;

    return 0;
}
